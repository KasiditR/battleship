﻿using UnityEngine;

namespace Singleton
{
    public class DynamicSingleton<T> : MonoBehaviour where T : MonoBehaviour
    {
        private static T _instance;

        public static T Instance
        {
            get
            {
                if (!_instance)
                {
                    _instance = FindObjectOfType<T>();
                    if (!_instance)
                    {
                        var singletonObject = new GameObject($"{typeof(T).Name} (Dynamic Singleton)");
                        _instance = singletonObject.AddComponent<T>();
                    }
                }

                return _instance;
            }
        }

        private void Awake()
        {
            if (_instance != null && _instance != this)
            {
                Destroy(gameObject);
                return;
            }

            _instance = GetComponent<T>();
            DontDestroyOnLoad(gameObject);
        }
    }
}