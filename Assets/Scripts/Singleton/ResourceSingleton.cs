﻿using UnityEngine;

namespace Singleton
{
    public class ResourceSingleton<T> : MonoBehaviour where T : MonoBehaviour
    {
        private static T _instance;

        public static T Instance
        {
            get
            {
                if (!_instance)
                {
                    _instance = Instantiate(Resources.Load<T>($"{typeof(T).Name}"));
                }

                return _instance;
            }
        }

        public virtual void Awake()
        {
            if (_instance != null && _instance != this)
            {
                Destroy(gameObject);
                return;
            }

            name = $"{typeof(T).Name} (Resources Singleton)";
            _instance = GetComponent<T>();
            DontDestroyOnLoad(gameObject);
        }
    }
}