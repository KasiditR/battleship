﻿using BattleShip.Manager;
using UnityEngine;

namespace BattleShip.State
{
    public class TransitionState : GameState
    {
        private readonly GameState _nextState;
        private float _delay;

        public TransitionState(GameManager gameManager, GameState nextState, float delay) : base(gameManager)
        {
            _nextState = nextState;
            _delay = delay;
        }

        protected override void Update()
        {
            base.Update();
            _delay -= Time.deltaTime;
            if (_delay < 0)
            {
                SetNextState(_nextState);
            }
        }
    }
}