﻿using BattleShip.UI;
using BattleShip.UI.Game;
using Singleton;
using UnityEngine;
using Utils;

namespace BattleShip.Factory
{
    public class HealthBarFactory : MonoSingleton<HealthBarFactory>
    {
        [SerializeField] private BarUI healthBarPrefab;

        public BarUI Create(Transform parent)
        {
            return Instantiate(healthBarPrefab, parent);
        }
    }
}