﻿using UnityEngine;

namespace BattleShip.Listener
{
    public abstract class BaseListener : MonoBehaviour
    {
        private void Start()
        {
            Initialize();
        }

        private void OnDestroy()
        {
            Uninitialized();
        }

        protected abstract void Initialize();
        protected abstract void Uninitialized();
    }
}