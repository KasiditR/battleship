﻿using BattleShip.Manager;
using BattleShip.UI.Game;
using UnityEngine;
using Utils;

namespace BattleShip.Listener
{
    public class GameListener : BaseListener
    {
        [Header("PAUSE MENU UI")] [SerializeField]
        private KeyCode toggleMenuKey;

        [SerializeField] private PauseMenuUI pauseMenuUI;

        [Header("PRE GAME UI")] [SerializeField]
        private PreGameUI preGameUI;

        [Header("END GAME UI")] [SerializeField]
        private EndGameUI endGameUI;

        private void Awake()
        {
            Debug.Assert(pauseMenuUI != null, "pauseMenuUI can't be null!");
            Debug.Assert(preGameUI != null, "preGameUI can't be null!");
            Debug.Assert(endGameUI != null, "endGameUI can't be null!");
        }

        protected override void Initialize()
        {
            GameManager.Instance.OnPreStarted += OnPreStartedEvent;
            GameManager.Instance.OnSetup += OnSetupEvent;
            GameManager.Instance.OnGameOver += OnGameOverEvent;
            GameManager.Instance.OnShipSetupModeChanged += OnShipSetupModeChangedEvent;
        }

        protected override void Uninitialized()
        {
            GameManager.Instance.OnPreStarted -= OnPreStartedEvent;
            GameManager.Instance.OnSetup -= OnSetupEvent;
            GameManager.Instance.OnGameOver -= OnGameOverEvent;
            GameManager.Instance.OnShipSetupModeChanged -= OnShipSetupModeChangedEvent;
        }

        private void OnPreStartedEvent()
        {
            preGameUI.Show();
            GameManager.Instance.TeamA.TeamRegion.Hide();
            GameManager.Instance.TeamB.TeamRegion.Hide();
        }

        private void OnSetupEvent()
        {
            preGameUI.Hide();
        }

        private void OnGameOverEvent(Team winner)
        {
            pauseMenuUI.Hide();
            JUtils.Value(0, 1, GameManager.Instance.GameConfig.RestartDelay).setOnComplete(() =>
                {
                    endGameUI.SetWinner(winner);
                    endGameUI.Show();
                }
            );
        }

        private void OnShipSetupModeChangedEvent(Team team, ObjectEditorManager.EditMode editMode)
        {
            if (editMode == ObjectEditorManager.EditMode.On)
            {
                team.TeamRegion.Show();
            }
            else
            {
                team.TeamRegion.Hide();
            }
        }

        private void Update()
        {
            if (Input.GetKeyDown(toggleMenuKey) && !GameManager.Instance.IsGameOver)
            {
                pauseMenuUI.Toggle();
            }
        }
    }
}