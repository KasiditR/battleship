﻿using BattleShip.Factory;
using BattleShip.Manager;
using BattleShip.System;
using BattleShip.UI;
using BattleShip.UI.Game;

namespace BattleShip.Listener
{
    public class TeamHealthBarListener : BaseListener
    {
        private BarUI[] _teamABar;
        private BarUI[] _teamBBar;

        protected override void Initialize()
        {
            GameManager.Instance.OnStarted += OnStartedEvent;
            GameManager.Instance.OnNextTurn += OnNextTurnEvent;
        }

        protected override void Uninitialized()
        {
            GameManager.Instance.OnStarted -= OnStartedEvent;
            GameManager.Instance.OnNextTurn -= OnNextTurnEvent;
        }

        private void OnStartedEvent()
        {
            SetBarTeam(GameManager.Instance.TeamA, out _teamABar);
            SetBarTeam(GameManager.Instance.TeamB, out _teamBBar);
        }

        private void OnNextTurnEvent(Team team)
        {
            UpdateTeamBar();
        }

        private void SetBarTeam(Team team, out BarUI[] teamBars)
        {
            teamBars = new BarUI[team.Members.Length];
            for (var i = 0; i < team.Members.Length; i++)
            {
                var ship = team.Members[i];
                var bar = HealthBarFactory.Instance.Create(ship.transform);

                ship.OnHealthChanged += (health) =>
                {
                    var clampedHealth = health / ship.MaxHealth;
                    bar.SetFillAmount(clampedHealth);
                };

                bar.Hide();
                teamBars[i] = bar;
            }
        }

        private void UpdateTeamBar()
        {
            var isATurn = GameManager.Instance.IsTeamATurn;

            foreach (var healthBar in _teamABar)
            {
                if (!healthBar) continue;
                if (isATurn) healthBar.Hide();
                else healthBar.Show();
            }

            foreach (var healthBar in _teamBBar)
            {
                if (!healthBar) continue;
                if (isATurn) healthBar.Show();
                else healthBar.Hide();
            }
        }
    }
}