﻿using System;
using BattleShip.Manager;
using BattleShip.Pool;
using BattleShip.Ship;
using UnityEngine;

namespace BattleShip.Listener
{
    public class EffectListener : BaseListener
    {
        [SerializeField] private EffectPool fireEffect;
        [SerializeField] private EffectPool hitEffect;
        [SerializeField] private EffectPool explodeEffect;
        [SerializeField] private EffectPool waterEffect;

        private void Awake()
        {
            Debug.Assert(fireEffect != null, "fireEffect can't be null!");
            Debug.Assert(hitEffect != null, "hitEffect can't be null!");
            Debug.Assert(explodeEffect != null, "explodeEffect can't be null!");
            Debug.Assert(waterEffect != null, "waterEffect can't be null!");
        }

        protected override void Initialize()
        {
            GameManager.Instance.OnShipFired += OnShipFiredEvent;
            GameManager.Instance.OnShipDestroyed += OnShipDestroyedEvent;
            GameManager.Instance.OnBulletHit += OnBulletHitEvent;
        }

        protected override void Uninitialized()
        {
            GameManager.Instance.OnShipFired -= OnShipFiredEvent;
            GameManager.Instance.OnShipDestroyed -= OnShipDestroyedEvent;
            GameManager.Instance.OnBulletHit -= OnBulletHitEvent;
        }

        private void OnShipFiredEvent(Bullet bullet)
        {
            var effect = fireEffect.Request();
            effect.transform.position = bullet.transform.position;
            effect.transform.rotation = GameManager.Instance.ActiveTeam.ActivePlayer.Cannon.transform.rotation;
            effect.gameObject.SetActive(false);
            effect.gameObject.SetActive(true);
        }

        private void OnShipDestroyedEvent(ShipBase ship)
        {
            var effect = explodeEffect.Request();
            effect.transform.position = ship.transform.position;
            effect.gameObject.SetActive(false);
            effect.gameObject.SetActive(true);
        }

        private void OnBulletHitEvent(HitType hitType, Vector3 point)
        {
            GameObject effect = null;
            switch (hitType)
            {
                case HitType.Water:
                    effect = waterEffect.Request();
                    break;
                case HitType.Ship:
                    effect = hitEffect.Request();
                    break;
            }

            if (!effect) return;
            effect.transform.position = point;
            effect.SetActive(false);
            effect.SetActive(true);
        }
    }
}