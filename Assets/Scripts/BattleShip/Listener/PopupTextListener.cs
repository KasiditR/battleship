﻿using BattleShip.Manager;
using BattleShip.UI.Game;

namespace BattleShip.Listener
{
    public class PopupTextListener : BaseListener
    {
        protected override void Initialize()
        {
            GameManager.Instance.OnNextTurn += OnNextTurnEvent;
            GameManager.Instance.OnGameOver += OnGameOverEvent;
            GameManager.Instance.OnShipSetupModeChanged += OnShipSetupModeChangedEvent;
        }

        protected override void Uninitialized()
        {
            GameManager.Instance.OnNextTurn -= OnNextTurnEvent;
            GameManager.Instance.OnGameOver -= OnGameOverEvent;
            GameManager.Instance.OnShipSetupModeChanged -= OnShipSetupModeChangedEvent;
        }

        private void OnNextTurnEvent(Team team)
        {
            PopupText.Instance.Show($"{team.TeamName}'S TURN");
        }

        private void OnGameOverEvent(Team winner)
        {
            PopupText.Instance.Show($"{winner.TeamName} IS THE WINNER!",
                GameManager.Instance.GameConfig.RestartDelay);
        }

        private void OnShipSetupModeChangedEvent(Team team, ObjectEditorManager.EditMode editMode)
        {
            if (editMode == ObjectEditorManager.EditMode.On)
            {
                PopupText.Instance.Show($"( ! ) DRAG OBJECT TO ADJUST POSITION");
                SoundManager.Play(SoundType.Warning);

            }
        }
    }
}