﻿using BattleShip.Manager;
using BattleShip.Ship;
using BattleShip.UI.Game;
using UnityEngine;

namespace BattleShip.Listener
{
    public class FuelListener : BaseListener
    {
        [SerializeField] private BarUI fuelBar;

        private void Awake()
        {
            Debug.Assert(fuelBar != null, "fuelBar can't be null!");
        }

        protected override void Initialize()
        {
            GameManager.Instance.OnNextTurn += OnNextTurnEvent;
            GameManager.Instance.OnGameOver += OnGameOverEvent;
        }

        protected override void Uninitialized()
        {
            GameManager.Instance.OnNextTurn -= OnNextTurnEvent;
            GameManager.Instance.OnGameOver -= OnGameOverEvent;
        }

        private ShipBase _currentShip;

        private void OnNextTurnEvent(Team team)
        {
            if (_currentShip) _currentShip.OnFuelChanged -= OnFuelChangedEvent;

            _currentShip = team.ActivePlayer;
            _currentShip.OnFuelChanged += OnFuelChangedEvent;

            fuelBar.Show();
        }

        private void OnFuelChangedEvent(float fuel)
        {
            var clampedFuel = fuel / _currentShip.MaxFuel;
            fuelBar.SetFillAmount(clampedFuel);
        }

        private void OnGameOverEvent(Team winner)
        {
            fuelBar.Hide();
        }
    }
}