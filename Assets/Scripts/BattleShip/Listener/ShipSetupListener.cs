﻿using BattleShip.Manager;
using BattleShip.System;
using BattleShip.UI;
using BattleShip.UI.Game;
using UnityEngine;

namespace BattleShip.Listener
{
    public class ShipSetupListener : BaseListener
    {
        [SerializeField] private ShipSetupUI setupUI;

        protected override void Initialize()
        {
            GameManager.Instance.OnShipSetupModeChanged += OnShipSetupModeChangedEvent;
        }

        protected override void Uninitialized()
        {
            GameManager.Instance.OnShipSetupModeChanged -= OnShipSetupModeChangedEvent;
        }

        private void OnShipSetupModeChangedEvent(Team team, ObjectEditorManager.EditMode editMode)
        {
            var isActive = editMode == ObjectEditorManager.EditMode.On;
            if (isActive)
            {
                setupUI.SetTeam(team);
                setupUI.Show();
            }
            else
            {
                setupUI.Hide();
            }
        }
    }
}