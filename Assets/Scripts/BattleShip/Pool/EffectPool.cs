﻿using UnityEngine;

namespace BattleShip.Pool
{
    public class EffectPool : MonoBehaviour, IPool<GameObject>
    {
        [SerializeField] private int initBullet;
        [SerializeField] private GameObject effectPrefab;

        private GameObject[] _effectPool;
        private int _poolIndex;

        public void Awake()
        {
            Debug.Assert(effectPrefab != null, "effectPrefab can't be null!");
            InitWarm(initBullet);
        }

        public void InitWarm(int amount)
        {
            _effectPool = new GameObject[amount];
            for (var i = 0; i < amount; i++)
            {
                var effect = Instantiate(effectPrefab);
                effect.gameObject.SetActive(false);
                _effectPool[i] = effect;
            }
        }

        public GameObject Request()
        {
            var effect = _effectPool[_poolIndex++ % _effectPool.Length];
            return effect;
        }

        public void Return(GameObject effect)
        {
            effect.gameObject.SetActive(false);
        }
    }
}