﻿using System;
using Singleton;
using UnityEngine;
using Utils;

namespace BattleShip.System
{
    public class QuickTime : MonoSingleton<QuickTime>, IVisible
    {
        [SerializeField] private bool reverseSpeed;
        [SerializeField] private bool useSinSpeed;
        [SerializeField] private float speed;

        public event Action<bool> OnVisibleChanged;
        public event Action<float> OnValueChanged;
        
        private QuickTimeEvents _quickTimeEvents;

        public override void Awake()
        {
            base.Awake();
            _quickTimeEvents = new QuickTimeEvents(speed, useSinSpeed, reverseSpeed);
            Hide();
        }

        private void Update()
        {
            _quickTimeEvents.Update();
            OnValueChanged?.Invoke(_quickTimeEvents.Value);
        }

        public float Check()
        {
            return _quickTimeEvents.Value;
        }

        public void Reset()
        {
            _quickTimeEvents.Reset();
        }

        public void Show()
        {
            gameObject.SetActive(true);
            OnVisibleChanged?.Invoke(true);
        }

        public void Hide()
        {
            gameObject.SetActive(false);
            OnVisibleChanged?.Invoke(false);
        }
    }
}