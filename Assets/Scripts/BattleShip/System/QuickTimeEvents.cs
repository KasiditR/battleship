﻿using UnityEngine;

namespace BattleShip.System
{
    public class QuickTimeEvents
    {
        public float Value { get; private set; }

        private readonly float _speed;
        private readonly bool _sinSpeed;
        private readonly bool _reverse;

        private bool _isReverse;

        public QuickTimeEvents(float speed, bool sinSpeed, bool reverse)
        {
            _speed = speed;
            _sinSpeed = sinSpeed;
            _reverse = reverse;
        }

        public void Reset()
        {
            Value = 0f;
            _isReverse = false;
        }

        public void Update()
        {
            var increase = Time.deltaTime * _speed * (_sinSpeed ? Mathf.Abs(Mathf.Sin(Time.time)) : 1);
            Value += _isReverse && _reverse ? -increase : increase;
            if (Value > 1f)
            {
                Value = 1f;
                _isReverse = true;
                if (!_reverse)
                    Value = 0f;
            }
            else if (Value < 0f)
            {
                Value = 0f;
                _isReverse = false;
            }
        }
    }
}