﻿using Cinemachine;
using Singleton;
using UnityEngine;
using Utils;

namespace BattleShip.System
{
    [RequireComponent(typeof(CinemachineVirtualCamera))]
    public class GameCamara : MonoSingleton<GameCamara>
    {
        private CinemachineVirtualCamera _cam;

        public override void Awake()
        {
            base.Awake();
            _cam = GetComponent<CinemachineVirtualCamera>();
        }

        public void LookAt(Transform target)
        {
            _cam.LookAt = target;
        }

        public void Follow(Transform target)
        {
            _cam.Follow = target;
        }
    }
}