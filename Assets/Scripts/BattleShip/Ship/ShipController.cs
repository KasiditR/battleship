﻿using UnityEngine;

namespace BattleShip.Ship
{
    public class ShipController : MonoBehaviour, IController
    {
        [SerializeField] private float moveSpeed;
        [SerializeField] private float turnSpeed;

        private Rigidbody _rigidbody;

        private float _vertical;
        private float _horizontal;

        public void SetHorizontal(float value)
        {
            _horizontal = value;
        }

        public void SetVertical(float value)
        {
            _vertical = value;
        }

        private void Awake()
        {
            _rigidbody = GetComponent<Rigidbody>();
        }

        private void FixedUpdate()
        {
            Boost();
            Turn();
        }

        private void Boost()
        {
            var velocity = transform.forward * (_vertical * moveSpeed);
            _rigidbody.AddForce(velocity * Time.fixedDeltaTime);
            _vertical = 0f;
        }

        private void Turn()
        {
            var torque = transform.up * (_horizontal * turnSpeed);
            _rigidbody.AddTorque(torque * Time.fixedDeltaTime);
            _horizontal = 0f;
        }
    }
}