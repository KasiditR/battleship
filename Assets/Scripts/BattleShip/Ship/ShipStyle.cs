﻿using UnityEngine;

namespace BattleShip.Ship
{
    public class ShipStyle : MonoBehaviour
    {
        [SerializeField] private MeshRenderer shipRenderer;

        public void SetStyle(Material material)
        {
            shipRenderer.material = material;
        }
    }
}