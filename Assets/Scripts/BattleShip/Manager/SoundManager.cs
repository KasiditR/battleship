﻿using Singleton;
using UnityEngine;
using Utils;

namespace BattleShip.Manager
{
    public static class SoundManager 
    {
        public static AudioSource Play(AudioClip audioClip, Vector3 position)
        {
            var audioSource = LeanAudio.playClipAt(audioClip, position);
            audioSource.spatialBlend = 1f;
            return audioSource;
        }

        public static AudioSource Play(AudioClip audioClip)
        {
            return LeanAudio.play(audioClip);
        }

        public static AudioSource Play(AudioClip[] audioClips, Vector3 position)
        {
            var audioSource = LeanAudio.playClipAt(audioClips[Random.Range(0, audioClips.Length)], position);
            audioSource.spatialBlend = 1f;
            return audioSource;
        }

        public static AudioSource Play(AudioClip[] audioClips)
        {
            return LeanAudio.play(audioClips[Random.Range(0, audioClips.Length)]);
        }

        public static AudioSource Play(SoundType soundType)
        {
            var audioClip = SoundAsset.Instance.GetSound(soundType);
            return audioClip ? LeanAudio.play(audioClip) : new AudioSource();
        }

        public static AudioSource Play(SoundType soundType, Vector3 position)
        {
            var audioClip = SoundAsset.Instance.GetSound(soundType);
            if (audioClip)
            {
                var audioSource = LeanAudio.playClipAt(audioClip, position);
                audioSource.spatialBlend = 1f;
                return audioSource;
            }

            return new AudioSource();
        }
    }
}