﻿using BattleShip.System;
using UnityEngine;
using Utils;

namespace BattleShip.Manager
{
    public class SplashScreenManager : MonoBehaviour
    {
        private void Start()
        {
            FadeScreen.FadeIn(1f)
                .setOnComplete(FadeOut);

            void FadeOut()
            {
                FadeScreen.FadeOut(1f)
                    .setOnComplete(() => { SceneLoader.LoadScene(SceneLoader.SceneType.MainMenu); });
            }
        }
    }
}