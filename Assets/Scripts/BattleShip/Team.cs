﻿using System.Linq;
using BattleShip.Map;
using BattleShip.Ship;
using UnityEngine;

namespace BattleShip
{
    public class Team
    {
        public bool IsShipSetup { get; set; }
        public string TeamName { get; }
        public Region TeamRegion { get; }
        public ShipBase[] Members { get; }
        public int Alive => GetAliveMembers().Length;

        public ShipBase ActivePlayer { get; private set; }

        public Team(string teamName, ShipBase[] members, Region region)
        {
            TeamName = teamName;
            Members = members;
            TeamRegion = region;
        }

        public void SetActiveMember(ShipBase shipBase)
        {
            ActivePlayer = shipBase;
        }

        public ShipBase GetRandomMember()
        {
            var liveMember = GetAliveMembers();
            return liveMember[Random.Range(0, liveMember.Length)];
        }

        public ShipBase[] GetAliveMembers()
        {
            var alive = Members.Where(s => !s.IsDead).ToArray();
            return alive == null ? new ShipBase[0] : alive;
        }

        public void SetFinishSetup()
        {
            IsShipSetup = true;
        }
    }
}