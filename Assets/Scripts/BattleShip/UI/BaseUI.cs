﻿using UnityEngine;

namespace BattleShip.UI
{
    public abstract class BaseUI : MonoBehaviour, IVisible
    {
        public void Show()
        {
            gameObject.SetActive(true);
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }
    }
}