﻿using BattleShip.Manager;
using BattleShip.System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Utils;

namespace BattleShip.UI.Main_Menu
{
    public class MapIcon : BaseUI
    {
        [SerializeField] private AudioClip selectLevel;
        [SerializeField] private Image coverImg;
        [SerializeField] private TextMeshProUGUI mapNameText;
        [SerializeField] private Button mapButton;

        private void Awake()
        {
            Debug.Assert(coverImg != null, "coverImg can't be null!");
            Debug.Assert(mapNameText != null, "mapNameText can't be null!");
            Debug.Assert(mapButton != null, "mapButton can't be null!");
        }

        public void Init(string mapName, Sprite mapCover, int sceneId)
        {
            SetMapName(mapName);
            SetMapCover(mapCover);
            mapButton.onClick.RemoveAllListeners();
            mapButton.onClick.AddListener(() =>
            {
                SoundManager.Play(selectLevel);
                FadeScreen.FadeOut(1f).setOnComplete(() => { SceneLoader.LoadScene(sceneId); });
            });
        }

        public void SetMapName(string mapName)
        {
            mapNameText.SetText(mapName);
        }

        public void SetMapCover(Sprite mapIcon)
        {
            coverImg.sprite = mapIcon;
        }
    }
}