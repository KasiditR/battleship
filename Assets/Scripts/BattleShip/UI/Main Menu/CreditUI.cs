﻿using BattleShip.Manager;
using BattleShip.Map.ScriptableObjects;
using UnityEngine;
using UnityEngine.UI;

namespace BattleShip.UI.Main_Menu
{
    public class CreditUI : BaseUI
    {
        [SerializeField] private Button openButton;
        [SerializeField] private Button closeButton;

        private void Awake()
        {
            Debug.Assert(openButton != null, "openButton can't be null!");
            Debug.Assert(closeButton != null, "closeButton can't be null!");
        }

        public void Init()
        {
            openButton.onClick.AddListener(() =>
            {
                Show();
                SoundManager.Play(SoundType.Button);
            });

            closeButton.onClick.AddListener(() =>
            {
                Hide();
                SoundManager.Play(SoundType.Button);
            });
        }
    }
}