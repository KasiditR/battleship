﻿using System;
using Singleton;
using TMPro;
using UnityEngine;
using Random = UnityEngine.Random;

namespace BattleShip.UI.Game
{
    public class PopupText : MonoSingleton<PopupText>
    {
        [SerializeField] private TextMeshProUGUI popupText;
        [SerializeField] private CanvasGroup overlay;

        private Action _onComplete;
        private LTDescr _ltDescr;

        public override void Awake()
        {
            base.Awake();
            Hide();

            Debug.Assert(popupText != null, "popupText can't be null!");
            Debug.Assert(overlay != null, "overlay can't be null!");
        }

        public void Show()
        {
            Cancel();

            overlay.alpha = 1;
            overlay.gameObject.SetActive(true);
        }

        public void Hide()
        {
            Cancel();

            overlay.alpha = 0;
            overlay.gameObject.SetActive(false);
        }

        public void Cancel()
        {
            if (_ltDescr != null)
            {
                LeanTween.cancel(_ltDescr.id);
            }
        }


        public PopupText OnComplete(Action action)
        {
            _onComplete = action;
            return this;
        }

        public PopupText Show(string info, float stayTime = 3f, float fadeInTime = 0.1f, float fadeOutTime = 0.5f)
        {
            _onComplete = () => { };

            stayTime -= fadeInTime;
            stayTime -= fadeOutTime;
            if (stayTime <= 0) return this;

            Show();

            popupText.SetText(info);

            //Fade In
            _ltDescr = LeanTween.value(0, 1, fadeInTime)
                .setOnUpdate((v) => { overlay.alpha = v; })
                .setOnComplete(Stay);

            //Stay
            void Stay()
            {
                _ltDescr = LeanTween.value(0, 1, stayTime)
                    .setOnComplete(FadeOut);
            }

            //Fade Out
            void FadeOut()
            {
                _ltDescr = LeanTween.value(1, 0, fadeOutTime)
                    .setOnUpdate((v) => { overlay.alpha = v; })
                    .setOnComplete(() =>
                    {
                        _onComplete?.Invoke();
                        Hide();
                    });
            }

            return this;
        }

        private void OnDestroy()
        {
            Cancel();
        }

        #region Test

#if UNITY_EDITOR
        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.P))
            {
                Show($"BURGER {Random.Range(1000, 100000)}");
            }
        }
#endif

        #endregion
    }
}