﻿using BattleShip.Manager;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace BattleShip.UI.Game
{
    public class PreGameUI : BaseUI
    {
        [SerializeField] private TextMeshProUGUI versusText;
        [SerializeField] private Button startButton;

        private void Awake()
        {
            Debug.Assert(versusText != null, "versusText can't be null!");
            Debug.Assert(startButton != null, "startButton can't be null!");

            startButton.onClick.AddListener(() =>
            {
                GameManager.Instance.Setup();
                GameManager.Instance.OnButtonClickEvent();
            });

            var teamA = GameManager.Instance.TeamA;
            var teamB = GameManager.Instance.TeamB;
            versusText.SetText($"{teamA.TeamName} VS {teamB.TeamName}");
        }
    }
}