﻿using UnityEngine;

namespace BattleShip.UI.Game
{
    public class ItemBarUI : BaseUI
    {
        [SerializeField] private Transform container;
        [SerializeField] private Transform itemTemplate;

        private void Awake()
        {
            Debug.Assert(container != null, "container can't be null!");
            Debug.Assert(itemTemplate != null, "itemTemplate can't be null!");

            itemTemplate.gameObject.SetActive(false);
        }

        private void Clear()
        {
            foreach (Transform child in container)
                child.gameObject.SetActive(false);
        }

        public void SetItemAmount(int amount)
        {
            Clear();

            var available = Mathf.Clamp(container.childCount, 0, amount);
            var initCount = amount - available;
            for (var i = 0; i < available; i++)
            {
                container.GetChild(i).gameObject.SetActive(true);
            }

            for (var i = 0; i < initCount; i++)
            {
                var item = Instantiate(itemTemplate, container);
                item.gameObject.SetActive(true);
            }
        }
    }
}