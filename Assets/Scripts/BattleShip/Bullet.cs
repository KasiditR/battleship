﻿using System;
using BattleShip.Pool;
using UnityEngine;

namespace BattleShip
{
    [RequireComponent(typeof(Rigidbody))]
    public class Bullet : Entity
    {
        public event Action<Collision> OnHit;
        public float Damage { get; set; }

        private Rigidbody _rigidbody;

        private void Awake()
        {
            _rigidbody = GetComponent<Rigidbody>();
        }

        public void Init(Vector3 origin, Vector3 velocity, float damage)
        {
            transform.position = origin;
            _rigidbody.velocity = velocity;
            Damage = damage;
            OnHit = null;
        }

        private void OnCollisionEnter(Collision other)
        {
            OnHit?.Invoke(other);

            //De-active bullet and return to pool
            BulletPool.Instance.Return(this);

            if (!other.transform.TryGetComponent<IDamageable>(out var damageable)) return;
            damageable.Damage(Damage);
        }
    }
}