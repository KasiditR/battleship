﻿using UnityEngine;

namespace BattleShip.Map
{
    public class Region : MonoBehaviour, IVisible
    {
        [SerializeField] private LayerMask regionLayerMask;
        [SerializeField] private Transform spawnPoint;
        [SerializeField] private Transform setupCamPoint;
        public Transform SpawnPoint => spawnPoint;
        public Transform SetupCamPoint => setupCamPoint;
        public LayerMask RegionLayerMask => regionLayerMask;

        private MeshRenderer _area;

        private void Awake()
        {
            _area = GetComponent<MeshRenderer>();
        }

        public void Show()
        {
            _area.enabled = true;
        }

        public void Hide()
        {
            _area.enabled = false;
        }
    }
}