﻿using UnityEngine;

namespace BattleShip.Map.ScriptableObjects
{
    [CreateAssetMenu(fileName = "New Map Config", menuName = "Map Config")]
    public class MapConfig : ScriptableObject
    {
        [SerializeField] private int sceneId;
        [SerializeField] private Sprite mapCover;
        [SerializeField] private string mapName;

        public int SceneId => sceneId;
        public Sprite MapCover => mapCover;
        public string MapName => mapName;
    }
}