using UnityEngine;

namespace BattleShip.Map
{
    public class WaterEffect : MonoBehaviour
    {
        [SerializeField] private float speed;
        private MeshRenderer _renderer;

        private void Awake()
        {
            _renderer = GetComponent<MeshRenderer>();
        }

        void FixedUpdate()
        {
            _renderer.material.mainTextureOffset = Vector2.up * (Mathf.Sin(Time.time) * speed);
        }
    }
}