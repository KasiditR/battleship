﻿using UnityEngine;

namespace BattleShip.ScriptableObjects
{
    [CreateAssetMenu(fileName = "New Game Config", menuName = "Game Config")]
    public class GameConfig : ScriptableObject
    {
        [SerializeField] private float transitionDelay;
        [SerializeField] private float restartDelay;
        public float TransitionDelay => transitionDelay;
        public float RestartDelay => restartDelay;
    }
}