﻿using UnityEngine;

namespace Utils
{
    public static class BitBoard
    {
        public static bool CheckLayerMask(LayerMask a, LayerMask b)
        {
            //Bitwise Operator
            return a == (a | (1 << b));
        }
    }
}